/* 
 * Copyright 2015-2017 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

function Scope() {
    this._globals = {};
    this._stack = [];
    return this;
}

/**
 * Add an interpreter listener.
 *
 * @param listener
 */
Scope.prototype.addScopeListener = function (listener) {};

/**
 * Add an interpreter listener.
 *
 * @param listener
 */
Scope.prototype.removeScopeListener = function (listener) {};

/**
 * Get the commands to interpret.
 *
 * @param name
 * @return
 */
Scope.prototype.get = function (name) {
    return this._globals[name];
};

/**
 * Get the commands to interpret.
 *
 * @param name
 * @param value
 * @return
 */
Scope.prototype.set = function (name, value) {};

/**
 * Get the commands to interpret.
 *
 * @param name
 * @param value
 * @return
 */
Scope.prototype.setGlobal = function (name, value) {
    this._globals[name] = value;
};

/**
 * Get the commands to interpret.
 *
 * @param name
 * @param value
 * @return
 */
Scope.prototype.setNew = function (name, value) {};


/**
 * Is the current block halted.
 *
 * @return true if halted, false if the monitor is null or the monitor is
 * not halted.
 */
Scope.prototype.pop = function () {
    var ret = this._stack[0];
    this._stack.splice(0, 1);
    return ret;
};

/**
 * Process the statement = function (s).
 *
 * @param block
 * @return true if we should continue, false otherwise
 */
Scope.prototype.push = function (block) {
    this._stack.splice(0, 0, block);
};

/**
 * Get a function if it is defined.
 *
 * @param name
 * @return
 */
Scope.prototype.getFunction = function (name) {};


/**
 * Get the names of local variables.
 *
 * @return
 */
Scope.prototype.localVariables = function () {};

/**
 * Get the lexical parent.
 *
 * @return
 */
Scope.prototype.variables = function () {};

Scope.prototype.stack = function () {
    return this._stack;
};

/**
 * Get the lexical parent.
 *
 * @param index
 * @return
 */
Scope.prototype.variablesPeek = function (index) {};

exports.Scope = Scope;