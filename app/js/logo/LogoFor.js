/* 
 * Copyright 2015-2017 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0  = function (the "License"){};
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var codeBlock = require('../torgo/CodeBlock');
var exp = require('./ExpressionListener');
var consts = require('../torgo/CodeConstants');

/**
 * LogoFor
 *
 * @param ctx
 * @returns {ExpressionListener}
 */
function LogoFor(ctx) {
    codeBlock.CodeBlock.call(this, ctx);
    return this;
}

var ForType = {
    DECREASE: 0,
    INCREASE: 1,
    UNDETERMINED: 2
};
Object.freeze(ForType);

// inherit default listener
LogoFor.prototype = Object.create(codeBlock.CodeBlock.prototype);
LogoFor.prototype.constructor = LogoFor;

LogoFor.prototype.process = function (scope) {

//        LOGGER.verbose(MessageFormat.format("[{0}]: Line: {1}, Start: {2}, End: {3}", ctx.getClass().getName(), ctx.getStart().getLine(), ctx.getStart().getStartIndex(), ctx.getStart().getStopIndex()));
    scope.push(this);
    codeBlock.CodeBlock.prototype.push.call(this);
//        super.variables.add(0, new HashMap<>());
//        listeners.fire().currStatement(this, scope);

    var fore = codeBlock.CodeBlock.prototype.getParserRuleContext.call(this);
    var variable = fore.name().STRING().getText();
    var evaluator = new exp.ExpressionListener(scope);

    var start = evaluator.evalCtx(fore.expression(0));
    var stop = evaluator.evalCtx(fore.expression(1));

    //Are we increasing/decreasing.
    //set the default step accordingly.
    var step;
    var type = ForType.INCREASE;
    if (start > stop) {
        type = ForType.DECREASE;
        step = 1.0;
    } else if (stop > start) {
        type = ForType.INCREASE;
        step = 1.0;
    } else {
        type = ForType.UNDETERMINED;
        step = 0.0;
    }

    //if the step value is specified, evalutate.
    if (fore.expression().length > 2) {
        step = evaluator.evalCtx(fore.expression(2));
    }

    //process and step
    var success = true;
    if (step !== 0) {
//            //not sure if this should be <=
        var doMore = type === ForType.INCREASE ? start < stop : stop < start;
        while (success && doMore) {
            scope.setNew(variable, start);
            var supCall = codeBlock.CodeBlock.prototype.process.call(this, scope);
            success = success && supCall === consts.SUCCESS;

            switch (type) {
                case ForType.INCREASE:
                    start += step;
                    break;
                case ForType.DECREASE:
                    start -= step;
                    break;
            }
//                //not sure if this should be <=
            doMore = type === ForType.INCREASE ? start < stop : stop < start;
        }
    }

//        super.variables.remove(0);
    codeBlock.CodeBlock.prototype.pop.call(this);
    scope.pop();

    return consts.SUCCESS;
};

exports.LogoFor = LogoFor;