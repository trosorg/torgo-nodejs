/* 
 * Copyright 2015-2017 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0  = function (the "License"){};
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var codeBlock = require('../torgo/CodeBlock');
var exp = require('./ExpressionListener');
var consts = require('../torgo/CodeConstants');

/**
 * LogoRepeat
 *
 * @param ctx
 * @returns {LogoRepeat}
 */
function LogoRepeat(ctx) {
    codeBlock.CodeBlock.call(this, ctx);
    return this;
}

// inherit default listener
LogoRepeat.prototype = Object.create(codeBlock.CodeBlock.prototype);
LogoRepeat.prototype.constructor = LogoRepeat;

LogoRepeat.prototype.process = function (scope) {
//        LOGGER.verbose(MessageFormat.format("[{0}]: Line: {1}, Start: {2}, End: {3}", ctx.getClass().getName(), ctx.getStart().getLine(), ctx.getStart().getStartIndex(), ctx.getStart().getStopIndex()));
    scope.push(this);
    codeBlock.CodeBlock.prototype.push.call(this);
//        super.variables.add(0, new HashMap<>());
//        listeners.fire().currStatement(this, scope);

    var evaluator = new exp.ExpressionListener(scope);
    var success = consts.SUCCESS;
    var ctx = codeBlock.CodeBlock.prototype.getParserRuleContext.call(this);
    var repeat = evaluator.evalCtx(ctx.expression());
    for (var ii = 0; ii < repeat && success === consts.SUCCESS; ii++) {
        //this sets the repcount variable for dereferencing in the block.
        scope.setNew(consts.REPCOUNT_VAR, ii + 1);
        success = codeBlock.CodeBlock.prototype.process.call(this, scope);
    }

//        super.variables.remove(0);
    codeBlock.CodeBlock.prototype.pop.call(this);
    scope.pop();
    return success;
};

exports.LogoRepeat = LogoRepeat;

