/* 
 * Copyright 2015-2017 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var logoProg = require('./LogoProg.js');
var logoStatement = require('./LogoStatement.js');
var logoIf = require('./LogoIf.js');
var codeFunction = require('../torgo/CodeFunction.js');
var logoFor = require('./LogoFor.js');
var logoRepeat = require('./LogoRepeat.js');
var logoListener = require('./antlr/LogoListener.js');

/**
 * LexicalListener class
 *
 * @returns {LexicalListener}
 */
function LexicalListener() {
    this._stack = [];
    this._blocks = [];
    logoListener.LogoListener.call(this); // inherit default listener
    return this;
}

// inherit default listener
LexicalListener.prototype = Object.create(logoListener.LogoListener.prototype);
LexicalListener.prototype.constructor = LexicalListener;

LexicalListener.prototype.enterProcedureDeclaration = function (ctx) {
    console.log("Found Procedure: " + ctx.name().getText());
    var stmnt = new codeFunction.CodeFunction(ctx);
    this._blocks.push(stmnt);
    this._stack[0].addFunction(stmnt);
    stmnt.setParent(this._stack[0]);
    this._stack.splice(0, 0, stmnt);
};

LexicalListener.prototype.exitProcedureDeclaration = function (ctx) {
    this._stack.splice(0, 1);
};

LexicalListener.prototype.enterDs = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("ds", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterCc = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("cc", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterPc = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("pc", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterFontname = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("fontname", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterFontstyle = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("fontstyle", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterFontsize = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("fontsize", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterProg = function (ctx) {
    this._stack.splice(0, 0, new logoProg.LogoProg(ctx));
};

LexicalListener.prototype.enterPrint_command = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("print", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterFd = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("fd", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterBk = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("bk", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterRt = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("rt", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterLt = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("lt", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterPu = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("pu", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterPd = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("pd", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterCs = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("cs", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterHt = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("ht", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterSt = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("st", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterHome = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("home", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterSetxy = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("setxy", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterProcedureInvocation = function (ctx) {
    var stmnt = new logoStatement.LogoStatement(ctx.name().getText(), ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};


LexicalListener.prototype.enterMake = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("make", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.enterLocalmake = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("localmake", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};


LexicalListener.prototype.enterIfe = function (ctx) {
    var stmnt = new logoIf.LogoIf(ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
    stmnt.setParent(this._stack[0]);
    this._stack.splice(0, 0, stmnt);
};

LexicalListener.prototype.exitIfe = function (ctx) {
    this._stack.splice(0, 1);
};

LexicalListener.prototype.enterStop = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("stop", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};


LexicalListener.prototype.enterFore = function (ctx) {
    var stmnt = new logoFor.LogoFor(ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
    stmnt.setParent(this._stack[0]);
    this._stack.splice(0, 0, stmnt);
};

LexicalListener.prototype.exitFore = function (ctx) {
    this._stack.splice(0, 1);
};

LexicalListener.prototype.enterRepeat = function (ctx) {
    var stmnt = new logoRepeat.LogoRepeat(ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
    stmnt.setParent(this._stack[0]);
    this._stack.splice(0, 0, stmnt);
};

LexicalListener.prototype.exitRepeat = function (ctx) {
    this._stack.splice(0, 1);
};

LexicalListener.prototype.enterPause = function (ctx) {
    var stmnt = new logoStatement.LogoStatement("pause", ctx);
    this._blocks.push(stmnt);
    this._stack[0].addCommand(stmnt);
};

LexicalListener.prototype.getEntryPoint = function () {
    return this._stack[0];
};

LexicalListener.prototype.getBlocks = function () {
    return this._blocks;
};

exports.LexicalListener = LexicalListener;