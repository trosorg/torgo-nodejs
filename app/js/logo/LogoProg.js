/* 
 * Copyright 2015-2017 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0  = function (the "License"){};
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var codeBlock = require('../torgo/CodeBlock');
var consts = require('../torgo/CodeConstants');

/**
 * LogoProg
 *
 * @param ctx
 * @returns {LogoProg}
 */
function LogoProg(ctx) {
    codeBlock.CodeBlock.call(this, ctx);
    return this;
}

// inherit default listener
LogoProg.prototype = Object.create(codeBlock.CodeBlock.prototype);
LogoProg.prototype.constructor = LogoProg;

LogoProg.prototype.process = function (scope) {
    var ret = consts.SUCCESS;
    scope.push(this);
    ret = codeBlock.CodeBlock.prototype.process.call(this, scope);
    scope.pop();
    return ret;
};

exports.LogoProg = LogoProg;